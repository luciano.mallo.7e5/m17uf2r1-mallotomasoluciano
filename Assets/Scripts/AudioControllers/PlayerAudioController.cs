using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAudioController : MonoBehaviour
{
    [SerializeField] private AudioClip _playerMovement;
    [SerializeField] private AudioClip _playerDeath;
    private AudioSource _audioSource;
    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    public void PlayPlayerMovementAudio() 
    {
        if (!_audioSource.isPlaying)
            _audioSource.PlayOneShot(_playerMovement);
    }
    public void PlayPlayerAttackAudio(AudioClip audioClip)
    {
            _audioSource.PlayOneShot(audioClip);
    }
    public void PlayPlayerDeathAudio()
    {
            _audioSource.PlayOneShot(_playerDeath);
    }

}
