using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ShowLevelsDone : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        switch (GameManager.LevelsDone)
        {
            case 1:
                GetComponent<Text>().text = "just\nLevels Done: 1";
                break;

            case 2:
                GetComponent<Text>().text = "Not bad but...\nLevels Done: 2";
                break;

            case 3:
                GetComponent<Text>().text = "You get it.\nLevels Done: 3";
                break;

            default:
                GetComponent<Text>().text = "Levels Done: 0";
                break;
        }

       
    }

   
}
