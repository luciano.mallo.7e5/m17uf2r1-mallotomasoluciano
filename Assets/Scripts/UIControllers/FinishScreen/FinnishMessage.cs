using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class FinnishMessage : MonoBehaviour
{

    private GameObject _highScoreMessage;
    private void Awake()
    {
        _highScoreMessage = GameObject.Find("NewHighScoreText");
        _highScoreMessage.SetActive(false);
        
    }
  
    private void OnEnable()
    {
        SaveData.NewHighScoreEvent += ShowNewHighScoreText;
    }

    // Update is called once per frame
    void Update()
    {

        GetComponent<Text>().text = GetMessage();
    }

    private string GetMessage()
    {
        if (GameManager.IsLevelFinished)
        {
            return "Well done";
        }

        else
        {
            return "You Fail";
        }
    }
    private void ShowNewHighScoreText() 
    {
        _highScoreMessage.SetActive(true);
    }
    private void OnDisable()
    {
        SaveData.NewHighScoreEvent -= ShowNewHighScoreText;
    }
}
