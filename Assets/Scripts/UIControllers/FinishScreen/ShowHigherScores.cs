using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;


public class ShowHigherScores : MonoBehaviour
{
    [SerializeField] private ScoreClass _scoreData = new ScoreClass();
    // Start is called before the first frame update
    void Start()
    {
        // Debug.Log(Application.persistentDataPath + "/ScoreData.json"); Where to find the JSON document.
        LoadFromJson();
        WithForeachLoop();
    }

    void WithForeachLoop()
    {
        List<int> topFiveScores = _scoreData.CharacterScore.OrderByDescending(scores => scores).Take(4).ToList();
       
        
        for (int j = 0; j < topFiveScores.Count(); ++j)
        {
            Transform child = transform.GetChild(j);

            for (int i = 0; i < child.childCount; ++i)
            {

                if (child.GetChild(i).name == "Score")
                {
                    child.GetChild(i).GetComponent<Text>().text = topFiveScores.ElementAt(j).ToString() != null ? topFiveScores.ElementAt(j).ToString() : "000";
                }
                if (child.GetChild(i).name == "Name")
                {
                    int NameIndex = _scoreData.CharacterScore.IndexOf(topFiveScores.ElementAt(j));
                    child.GetChild(i).GetComponent<Text>().text = _scoreData.CharacterName.ElementAt(NameIndex) != null ? _scoreData.CharacterName.ElementAt(NameIndex) : " ";
                }
            }
        }

    }
    public void LoadFromJson()
    {
        ScoreClass score = JsonUtility.FromJson<ScoreClass>(ReadJsonFile());
        _scoreData.CharacterName = score.CharacterName;
        _scoreData.CharacterScore = score.CharacterScore;
    }
    private string ReadJsonFile()
    {
        return System.IO.File.ReadAllText(Application.persistentDataPath + "/ScoreData.json");
    }
}
