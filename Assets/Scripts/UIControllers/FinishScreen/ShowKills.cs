using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowKills : MonoBehaviour
{
    void Start()
    {
        GetComponent<Text>().text ="Kills: "+ GameManager.EnemiesDefeated.ToString();
    }
}
