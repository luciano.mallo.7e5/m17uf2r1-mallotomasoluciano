using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowShop : MonoBehaviour
{
    [SerializeField] private UI_Shop _uishop;
    // private bool _isActive=false;
    private void Awake()
    {
        _uishop.Hide();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.U))
        {
            if (GameManager.GameState == GameStates.Running)
            {

                GameManager.GameState = GameStates.Pause;
                Time.timeScale = 0;
                IShopCustomer shopCustomer = GameObject.FindGameObjectWithTag("Player").GetComponent<IShopCustomer>();
                _uishop.Show(shopCustomer);
            }
            else
            {
                GameManager.GameState = GameStates.Running;
                Time.timeScale = 1;
                _uishop.Hide();
            }
        }
       
    }
}
