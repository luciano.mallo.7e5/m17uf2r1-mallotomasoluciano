using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowInventary : MonoBehaviour
{
    public Sprite NoWeaponSprite;
    private Transform _inventoryBar;
    [SerializeField] private List<ItemSo> _playerInventory;
    private void Awake()
    {
        _playerInventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().inventory;
        _inventoryBar = transform.Find("InventoryBar");

    }

    // Start is called before the first frame update
    void Start()
    {
        ShowInventory();
    }

    // Update is called once per frame
    void Update()
    {
        _playerInventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().inventory;
        ShowInventory();
    }


    private void ShowInventory()
    {

        for (int j = 0; j < _inventoryBar.childCount; ++j)
        {
            // Reviewwww
            Transform slot = _inventoryBar.transform.GetChild(j);

            //for (int i = 0; i < slot.childCount; ++i)
            foreach (Transform item in slot.transform)
            {
                //Transform Imagechild = slot.GetChild(i);

                try
                {
                    item.GetComponent<Image>().sprite = _playerInventory[j].ItemSprite;
                }
                catch
                {
                    item.GetComponent<Image>().sprite = NoWeaponSprite;
                }

            }
        }



    }
}
