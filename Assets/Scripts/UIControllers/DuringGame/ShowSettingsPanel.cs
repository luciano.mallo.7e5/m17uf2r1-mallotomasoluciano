using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ShowSettingsPanel : MonoBehaviour
{
    [SerializeField] private GameObject Panel;
    [SerializeField] private Button _closeButton;

    private void Awake()
    {
        _closeButton.onClick.AddListener(ManageTheGameState);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ManageTheGameState();
        }
    }
    private void ManageTheGameState() 
    {
        
            if (GameManager.GameState == GameStates.Running)
            {

                GameManager.GameState = GameStates.Pause;
                Time.timeScale = 0;
                ToggleSettingPanel();
            }
            else
            {

                GameManager.GameState = GameStates.Running;
                Time.timeScale = 1;
                ToggleSettingPanel();
            }
    
    }
    void ToggleSettingPanel()
    {
        if (Panel.activeInHierarchy)
        {
            Panel.SetActive(false);
        }
        else
        {
            Panel.SetActive(true);
        }
    }
  
}
