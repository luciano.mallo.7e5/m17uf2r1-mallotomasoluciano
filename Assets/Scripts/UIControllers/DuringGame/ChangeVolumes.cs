using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeVolumes : MonoBehaviour
{

    [SerializeField] private Slider _masterSlider;
    [SerializeField] private Slider _fxSlider;
    [SerializeField] private Slider _musicSlider;

    private void Update()
    {
        SetMasterVolume(_masterSlider);
        SetMusicVolume(_musicSlider);
        SetFXVolume(_fxSlider);
    }
    public void SetMasterVolume(Slider volume)
    {
        GameObject.Find("AudioManager").GetComponent<AudioManager>().SetMasterVolume(volume);
    }

    public void SetMusicVolume(Slider volume)
    {
        GameObject.Find("AudioManager").GetComponent<AudioManager>().SetMusicVolume(volume);
    }

    public void SetFXVolume(Slider volume)
    {
        GameObject.Find("AudioManager").GetComponent<AudioManager>().SetFXVolume(volume);
    }

}
