using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GetUsetName : MonoBehaviour
{
    private Button _getUserName;
    private string _playerName;

    private void Awake()
    {
        GameManager.ActualLevel = GameActualLevel.StartMenu;
    }
    void Start()
    {

        _getUserName = GameObject.Find("GetPlayerName").GetComponent<Button>();

        _getUserName.onClick.AddListener(startGame);
    }

    private void startGame()
    {
        _playerName = GameObject.Find("PlayerName").GetComponent<Text>().text;
        GameManager.PlayerName = _playerName;
        GameManager.ActualLevel = GameActualLevel.FirstLevel;
        SceneManager.LoadScene("LevelAutoCreated");
    }
}
