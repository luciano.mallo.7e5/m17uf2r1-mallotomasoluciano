using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowHelpPanel : MonoBehaviour
{
    private Button _helpButton;
    [SerializeField] private GameObject Panel;
    [SerializeField] private Button _closeButton;

    private void Awake()
    {
        _helpButton = GetComponent<Button>();
        _helpButton.onClick.AddListener(ToggleHelpPanel);
        _closeButton.onClick.AddListener(ToggleHelpPanel);
    }

    void ToggleHelpPanel()
    {
        if (Panel.activeInHierarchy)
        {
            Panel.SetActive(false);
        }
        else
        {
            Panel.SetActive(true);
        }
    }
}
