using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowSettingPanel : MonoBehaviour
{
    private Button _settingButton;
    [SerializeField] private GameObject Panel;
    [SerializeField] private Button _closeButton;

    private void Awake()
    {
        _settingButton = GetComponent<Button>();
        _settingButton.onClick.AddListener(ToggleSettingPanel);
        _closeButton.onClick.AddListener(ToggleSettingPanel);
    }

    void ToggleSettingPanel()
    {
        if (Panel.activeInHierarchy)
        {
            Panel.SetActive(false);
        }
        else
        {
            Panel.SetActive(true);
        }
    }
}
