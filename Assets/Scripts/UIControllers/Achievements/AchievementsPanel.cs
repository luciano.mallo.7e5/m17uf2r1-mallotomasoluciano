using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementsPanel : MonoBehaviour
{
   [SerializeField] private GameObject _panel;
    private List<string> _achievements = new List<string>();
    // private bool _isPause;

    private void Awake()
    {
        _panel = GameObject.Find("AchievementsPanel");
        _panel.SetActive(false);
    }
   
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.I) )
        {
            if (GameManager.GameState==GameStates.Running) 
            {
                //_isPause = true;
                GameManager.GameState = GameStates.Pause;
                Time.timeScale = 0;
                ShowPanel();
            }
            else 
            {
                GameManager.GameState = GameStates.Running;
                Time.timeScale = 1;
                _panel.SetActive(false);
            }
        }
    }

    private void ShowPanel()
    {
        
        _panel.SetActive(true);
        CheckAchievementsToShow();

    }

    private void CheckAchievementsToShow() 
    {
        _achievements = GameManager.AchievementsFinnished;
        foreach (var achievement in _achievements)
        {
            if (GameObject.Find(achievement))
            {
                
                GameObject.Find(achievement).GetComponent<Image>().color = Color.white;
            }
            
        }
    }

}
