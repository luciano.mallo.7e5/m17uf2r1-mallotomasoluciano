using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowAchievementAdvise : MonoBehaviour
{
    private GameObject advise;
    public Animator animator;
    private void OnEnable()
    {
        advise = GameObject.Find("AchievementAdvise");
        AchievementsManager.AchievementCompleted += ShowAdvise;
        advise.SetActive(false);
    }
    private void OnDisable()
    {
        AchievementsManager.AchievementCompleted -= ShowAdvise;
        advise.SetActive(false);
    }
    void ShowAdvise() 
    {
        advise.SetActive(true);
        
        StartCoroutine("HideAdvise");
    }

    IEnumerator HideAdvise() 
    {
        animator.SetBool("Move", true);
        yield return new WaitForSeconds(12);
        advise.SetActive(false);
        animator.SetBool("Move", false);
    }
}

