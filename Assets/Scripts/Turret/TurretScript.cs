using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretScript : Enemy
{

    public GameObject alarmLight;
    public GameObject _TurretBulletPrefab;
    public GameObject _turretGun;
    [SerializeField] private float _range = 10f;
    private Transform _target;
    private bool _detected = false;
    private Vector2 _direction;
    [SerializeField] private float _fireRate = 3f;
    private float _nextTimeToShoot = 0;
    [SerializeField] private float _bulletForce = 100f;
    private bool _defeat;

    // Start is called before the first frame update
    void Start()
    {
        this.pointsByDefeating = 100;
        coinsByDefeating = 20;
        MaxHealth = 100;
        actualHealth = MaxHealth;
        damage = 10;
        _target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (!_defeat)
        {
            LookForInvaders();
            UpdateAliveState();
        }
    }
    private void LookForInvaders()
    {

        Vector2 targetpos = _target.position;
        _direction = targetpos - (Vector2)transform.position;
        RaycastHit2D rayInfo = Physics2D.Raycast(transform.position, _direction, _range);
        if (rayInfo)
        {

            if (rayInfo.collider.gameObject.tag == "Player")
            {

                if (!_detected)
                {
                    _detected = true;
                    alarmLight.GetComponent<SpriteRenderer>().color = Color.red;
                }
            }
        }
        else
        {

            if (_detected)
            {
                _detected = false;
                alarmLight.GetComponent<SpriteRenderer>().color = Color.green;
            }
        }
        if (_detected)
        {
            transform.up = _direction;
            if (Time.time > _nextTimeToShoot)
            {
                _nextTimeToShoot = Time.time + 1 / _fireRate;
                Shoot();
            }


        }
    }

    private void Shoot()
    {

        GameObject bullet = Instantiate(_TurretBulletPrefab, _turretGun.transform.position, Quaternion.identity);
        bullet.GetComponent<TurretBullet>().damage = damage;
        bullet.GetComponent<Rigidbody2D>().AddForce(_direction * _bulletForce, ForceMode2D.Impulse);


    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, _range);
    }
    private void UpdateAliveState()
    {
        if (actualHealth <= 0)
        {
            
                _defeat = true;
                base.Die();
                Destroy(gameObject,DyingSound.length);
            
        }
    }


}
