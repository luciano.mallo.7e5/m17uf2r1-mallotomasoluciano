using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretBullet : MonoBehaviour
{
    public int damage;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player") 
        {
            GameObject PlayerGO = collision.gameObject;
            PlayerGO.GetComponent<IDamageble>().RecieveDamage(damage);
            
        }
        Destroy(gameObject);
    }

}
