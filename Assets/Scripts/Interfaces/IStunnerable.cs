using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IStunnerable
{
    void Stun(float time);
}
