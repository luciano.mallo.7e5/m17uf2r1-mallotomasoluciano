using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Dash : MonoBehaviour
{

    private float dashingPower = 50f;
    private float dashCooldown = 1f;


    private Rigidbody2D _rigidBody;

    private float _actualCoolDowntime;

    void Start()
    {
        _rigidBody = GetComponent<Rigidbody2D>();

        _actualCoolDowntime = 0;
    }

    void Update() 
    {
        _actualCoolDowntime -= Time.deltaTime;
    }

    public void DoDash(Vector3 direction)
    {
        if (_actualCoolDowntime <= 0)
        {
            _rigidBody.AddForce(direction * dashingPower,ForceMode2D.Impulse);
            _actualCoolDowntime = dashCooldown;
        }    
    }
}




