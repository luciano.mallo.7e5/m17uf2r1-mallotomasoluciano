using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SaveData : MonoBehaviour
{
    [SerializeField] private ScoreClass _scoreData = new ScoreClass();
    public delegate void NewHighScore();
    public static event NewHighScore NewHighScoreEvent;
    private int _lastBestScore;
    private void Awake()
    {
        
            LoadFromJson();
            SaveIntoJson();
        
    }

    private void Start()
    {
        // Where to check if the JSON document is created correctly.
       // Debug.Log(Application.persistentDataPath + "/ScoreData.json");

    }
    private void Update()
    {
        CheckToThrowTheNewHighScoreEvent();
    }
    private void CheckToThrowTheNewHighScoreEvent()
    {
        if (GameManager.PlayerPoints > _lastBestScore)
        {    if (NewHighScoreEvent != null)
            {
                NewHighScoreEvent();
            }
    }
    }

    public void SaveIntoJson()
    {
        _scoreData.CharacterName.Add(GameManager.PlayerName);
        _scoreData.CharacterScore.Add(GameManager.PlayerPoints);
        string score = JsonUtility.ToJson(_scoreData);
        System.IO.File.WriteAllText(Application.persistentDataPath + "/ScoreData.json", score);
    }
    public void LoadFromJson()
    {
        ScoreClass score = JsonUtility.FromJson<ScoreClass>(ReadJsonFile());
        _scoreData.CharacterName = score.CharacterName;
        _scoreData.CharacterScore = score.CharacterScore;
        _lastBestScore = _scoreData.CharacterScore.Max();



    }
    private string ReadJsonFile()
    {
        return System.IO.File.ReadAllText(Application.persistentDataPath + "/ScoreData.json");
    }
}

[System.Serializable]
public class ScoreClass
{
    public List<string> CharacterName;
    public List<int> CharacterScore;
   
}
