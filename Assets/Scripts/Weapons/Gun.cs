using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : Weapon
{

    public Vector3 moveDirection;

    private void Awake()
    {
        _actualAmmo = 30;
        _totalAmmo = 30;
    }

    void Start()
    {
        name = "Gun";
        _actualAmmo = 6;
        _totalAmmo = 30;
        _fireRate = 0.5f;
        _rowAmmo = 6;
    }

    // Update is called once per frame
    void Update()
    {
        FollowPointer();
        Reload();
        Shoot();
    }


    public override void Reload()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            
       
            if (_totalAmmo > 0)
            {
                if (_actualAmmo == 0)
                {
                    _actualAmmo = _rowAmmo;
                    _totalAmmo -= _rowAmmo;
                }
                else
                {
                    _totalAmmo -= _rowAmmo - _actualAmmo;
                    _actualAmmo += _rowAmmo - _actualAmmo;
                }
            }

        }
    }
    public override void Shoot()
    {
        _nextTimeToShoot += Time.deltaTime;
        if (Input.GetMouseButton(0))
        {
            if (_actualAmmo > 0)
            {
                if (_nextTimeToShoot > _fireRate)
                {
                    _actualAmmo--;
                    Instantiate(_bullet, transform.position, Quaternion.identity);
                    _nextTimeToShoot = 0;

                }
            }
            else
            {
                Reload();
            }
        }
    }

    // Start is called before the first frame update


    private void FollowPointer()
    {
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 direction = mousePosition - transform.position;
        float _angle = Vector2.SignedAngle(Vector2.right, direction);
        transform.eulerAngles = new Vector3(0, 0, _angle);
    }


}
