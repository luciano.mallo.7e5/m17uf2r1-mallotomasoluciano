using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : MonoBehaviour
{
    public GameObject _bullet;
    public new string name;
    public float _actualAmmo;
    public float _totalAmmo;
    public float _rowAmmo;
    public float _maxAmmo;
    public float _fireRate;
    public float _reloadTime;
    public bool _isReloading;
    public float _nextTimeToShoot;


    public abstract void Shoot();
    public abstract void Reload();



}
