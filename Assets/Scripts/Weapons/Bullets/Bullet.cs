using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    public float _force;
    public int damage;


    // Start is called before the first frame update
    void Start()
    {
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 direction = mousePosition - transform.position;
        GetComponent<Rigidbody2D>().AddForce(direction.normalized * _force, ForceMode2D.Impulse);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        switch (collision.collider.tag)
        {
            case "Boss":
            case "Enemy":
                GameObject enemyGO = collision.gameObject;
                Vector2 direction = (Vector2)enemyGO.transform.position - (Vector2)transform.position;
                enemyGO.GetComponent<Rigidbody2D>().AddForce(direction * _force, ForceMode2D.Impulse);
                IDamageble character = enemyGO.GetComponent<IDamageble>();
                character.RecieveDamage(damage);
                Destroy(gameObject);
                break;
            case "Player":
                GetComponent<Collider2D>().isTrigger = true;
                break;

            case "EnemyBullet":
                break;

            default:
                Destroy(gameObject);
                break;
        }



    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            GetComponent<Collider2D>().isTrigger = false;
        }
    }

}
