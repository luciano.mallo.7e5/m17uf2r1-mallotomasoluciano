using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBullets : MonoBehaviour
{

    private GameObject _player;
    private Rigidbody2D _rb;
    private float _bulletForce = 5f;
    private int _damage = 20;
    private float _impulseForce = 10f;

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _player = GameObject.FindGameObjectWithTag("Player");
        Vector3 direction = _player.transform.position - transform.position;
        _rb.velocity = new Vector2(direction.x, direction.y).normalized * _bulletForce;
        float rot = Mathf.Atan2(-direction.y, -direction.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0, 0, rot + 90);

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

      

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player" || collision.tag == "Bullet")
        {
            if (collision.tag == "Player")
            {

                GameObject PlayerGO = collision.gameObject;
                PlayerGO.GetComponent<IDamageble>().RecieveDamage(_damage);
                Vector2 direction = (Vector2)PlayerGO.transform.position - (Vector2)transform.position;
                PlayerGO.GetComponent<Rigidbody2D>().AddForce(direction * _impulseForce, ForceMode2D.Impulse);
                Destroy(gameObject);
            }



        }

        else if(collision.tag != "Boss")
        {

            Destroy(gameObject);
        }
    }
}
