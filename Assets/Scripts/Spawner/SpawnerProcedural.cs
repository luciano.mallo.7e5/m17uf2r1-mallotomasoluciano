using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;


public class SpawnerProcedural : MonoBehaviour
{
    public static SpawnerProcedural SpawnerProceduralSingleton;
    [SerializeField] private List<GameObject> enemiesSpawned;
    public GameObject[] objectsToSpawn;
    private Vector3 _initposition;
    private int numOfEnemies;
    private GameActualLevel actualLevel;

    private void Awake()
    {
        if (SpawnerProceduralSingleton == null)
        {
            SpawnerProceduralSingleton = this;
        }
        else
        {
            DestroyImmediate(gameObject);
        }




    }
    private void Start()
    {
        actualLevel = GameManager.ActualLevel;
        CheckLevel();
        while (numOfEnemies != 0)
        {
            SpawnObject();
            numOfEnemies--;

        }
        Destroy(gameObject);
    }
    private void CheckLevel()
    {
        switch (actualLevel)
        {
            case GameActualLevel.FirstLevel:
                numOfEnemies = 5;
                break;
            case GameActualLevel.SecondLevel:
                numOfEnemies = 10;
                break;
            case GameActualLevel.ThirdLevel:
                numOfEnemies = 15;
                break;
        }
    }

    private void SpawnObject()
    {
        CheckWhereCanBeSpawn();
        GameObject enemyToBeSpawn = objectsToSpawn[Random.Range(0, objectsToSpawn.Length)];
        enemiesSpawned.Add(enemyToBeSpawn);
        Instantiate(enemyToBeSpawn, _initposition, enemyToBeSpawn.transform.rotation);
    }
    private void CheckWhereCanBeSpawn()
    {
        GameObject[] RoomsGO = GameObject.FindGameObjectsWithTag("Room");
        GameObject RoomGO = RoomsGO[Random.Range(0, RoomsGO.Length)];
        Bounds bounds = GetMaxBounds(RoomGO);
        do
        {
            float spawnPointX = bounds.center.x + Random.Range(-bounds.extents.x / 2, bounds.extents.x / 2);
            float spawnPointY = bounds.center.y + Random.Range(-bounds.extents.y / 2, bounds.extents.y / 2);
            _initposition = new Vector2(spawnPointX, spawnPointY);
        }
        while (Physics2D.CircleCast(_initposition, 1, Vector3.forward).collider != null);
    }
    private Bounds GetMaxBounds(GameObject g)
    {
        var renderers = g.GetComponentsInChildren<Renderer>();
        if (renderers.Length == 0) return new Bounds(g.transform.position, Vector3.zero);
        var b = renderers[0].bounds;
        foreach (Renderer r in renderers)
        {
            b.Encapsulate(r.bounds);
        }
        return b;
    }



}
