using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class SpawnerNoProcedural : Enemy
{
    [SerializeField] private List<GameObject> enemiesSpawned;
    public GameObject[] objectsToSpawn;
[SerializeField] private float timeToSpawn = 5f;
private float currentTimeToSpawn;
private Vector3 _initposition;
[SerializeField] private float _spawningTime = 40f;



private void Start()
{
    currentTimeToSpawn = timeToSpawn;
    SpawnObject();
}
private void Update()
{


    if (_spawningTime > 0)
    {
        _initposition = transform.position;
        CountTimeForNexTInstantiate();
        _spawningTime -= Time.deltaTime;
    }
    else
    {
        base.Die();
        Destroy(gameObject);
    }
}
private void CountTimeForNexTInstantiate()
{
    if (currentTimeToSpawn > 0)
    {
        currentTimeToSpawn -= Time.deltaTime;
    }
    else
    {
        SpawnObject();
        currentTimeToSpawn = timeToSpawn;
    }

}
private void SpawnObject()
{
    CheckWhereCanBeSpawn();
    GameObject enemyToBeSpawn = objectsToSpawn[Random.Range(0, objectsToSpawn.Length)];
    enemiesSpawned.Add(enemyToBeSpawn);
    Instantiate(enemyToBeSpawn, _initposition, enemyToBeSpawn.transform.rotation);
}
private void CheckWhereCanBeSpawn()
{
    GameObject WallsGO = GameObject.Find("Walls");
    Bounds bounds = WallsGO.GetComponent<Tilemap>().localBounds;
    do
    {
        float spawnPointX = bounds.center.x + Random.Range(-bounds.extents.x / 2, bounds.extents.x / 2);
        float spawnPointY = bounds.center.y + Random.Range(-bounds.extents.y / 2, bounds.extents.y / 2);
        _initposition = new Vector2(spawnPointX, spawnPointY);
    }
    while (Physics2D.CircleCast(_initposition, 1, Vector3.forward).collider != null && Vector2.Distance(_initposition, (Vector2)GameManager.PlayerCurrentPosition) > 4);
}

   
            
    
}

