using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageLootObjects : MonoBehaviour
{
    public string name;
    // public ItemSo item;
    private GameObject _player;
    private void Awake()
    {
        _player = GameObject.FindGameObjectWithTag("Player");
    }
    // Start is called before the first frame update
    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.tag == "Player")
        {
            switch (name)
            {
                case "HealthPotion":
                    AddHp(20);
                    break;
                case "GoldCoin":
                    AddPoints(50);
                    GameManager.GoldCoins += 10;
                    break;
                case "Gun":
                    AddAmmo();
                    break;
                
                default:
                    break;
            }
            Destroy(gameObject);

        }
    }

    private void AddHp(int hp)
    {
        
        if (GameManager.PlayerHealth < _player.GetComponent<Character>().MaxHealth)
        {
           
            if (GameManager.PlayerHealth + hp > _player.GetComponent<Character>().MaxHealth)
            {

                _player.GetComponent<Character>().actualHealth = _player.GetComponent<Character>().MaxHealth;
            }
            else
            {

                _player.GetComponent<Character>().actualHealth += hp;
                
            }
        }
    }
    private void AddPoints(int points)
    {
        GameManager.PlayerPoints += points;
    }
    private void AddAmmo()
    {
        WeaponController weaponController = GameObject.Find("WeaponGO").GetComponent<WeaponController>();
        string weaponName = weaponController.GetWeaponName();
        if (weaponName == "Gun")
        {
            weaponController.AddAmmo(20);
        }
    }
}
