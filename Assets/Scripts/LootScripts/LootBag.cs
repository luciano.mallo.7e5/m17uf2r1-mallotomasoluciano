using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootBag : MonoBehaviour
{

    public GameObject droppedItemPrefab;
    public List<ItemSo> lootList = new List<ItemSo>();

    // Start is called before the first frame update
    ItemSo GetDroppedItem()
    {
        int randomNum = Random.Range(1, 101);
        List<ItemSo> possibleItems = new List<ItemSo>();
        foreach (ItemSo item in lootList)
        {

            if (randomNum <= item.dropChance)
            {
                possibleItems.Add(item);
            }
        }
        if (possibleItems.Count > 0)
        {
            ItemSo droppedItem = possibleItems[Random.Range(0, possibleItems.Count)];
            return droppedItem;
        }
        Debug.Log("No loot Dropped");
        return null;


    }

    public void InstaiateLoot(Vector3 spawnPosition)
    {
        ItemSo droppedItem = GetDroppedItem();
        if (droppedItem != null)
        {
            GameObject lootGameObject = Instantiate(droppedItemPrefab, spawnPosition, Quaternion.identity);
            lootGameObject.GetComponent<ManageLootObjects>().name = droppedItem.Name;
            // lootGameObject.GetComponent<ManageLootObjects>().Item = droppedItem; // To do
            lootGameObject.GetComponent<SpriteRenderer>().sprite = droppedItem.ItemSprite;
        }
    }


}
