using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : Enemy
{
    private Vector3 _initposition;
    private float _timeToTeleport = 3f;
    private float _actualTimeToTeleport = 0;
    public GameObject _fireball;
    public Animator Animator;
    private bool _defeat;
    private float _timeToShoot=1f;
    private bool _canShoot=true;

    // Start is called before the first frame update
    void Start()
    {
        this.pointsByDefeating = 100;
        this.coinsByDefeating = 30;
        this.name = "Mage";
        this.actualHealth = 80;
    }

    // Update is called once per frame
    void Update()
    {
        ShootFireball();
        Teleport();
        Die();
    }


    private void Teleport()
    {
        if (_timeToTeleport > _actualTimeToTeleport)
        {
            _actualTimeToTeleport += Time.deltaTime;
        }
        else
        {
            _actualTimeToTeleport = 0;
            CheckWhereTeleport();
        }
    }

    public override void Die()
    {

        if (actualHealth <= 0)
        {
            if (!_defeat)
            {
                _defeat = true;
                Animator.SetBool("IsDead", true);
                base.Die();
                Destroy(gameObject, Animator.GetCurrentAnimatorStateInfo(0).length);
            }
        }
    }

    private void ShootFireball()
    {
        if (_canShoot) 
        { 
            Vector3 staffPosition = GameObject.Find("Staff").transform.position;
            Instantiate(_fireball, staffPosition, Quaternion.identity);
            StartCoroutine(WaitForShootAgain());
        }
    }
    IEnumerator WaitForShootAgain()
    {

        _canShoot = false;

        yield return new WaitForSeconds(_timeToShoot);

        _canShoot = true;
    }
    private void CheckWhereTeleport()
    {
        GameObject RoomGO = GameObject.Find("BossRoom");
        Bounds bounds = GetMaxBounds(RoomGO);
        do
        {
            float spawnPointX = bounds.center.x + Random.Range(-bounds.extents.x / 2, bounds.extents.x / 2);
            float spawnPointY = bounds.center.y + Random.Range(-bounds.extents.y / 2, bounds.extents.y / 2);
            _initposition = new Vector2(spawnPointX, spawnPointY);
        }
        while (Physics2D.CircleCast(_initposition, 1, Vector3.forward).collider != null);
        transform.position = new Vector3(_initposition.x, _initposition.y, 0);
    }

    private Bounds GetMaxBounds(GameObject g)
    {
        var renderers = g.GetComponentsInChildren<Renderer>();
        if (renderers.Length == 0) return new Bounds(g.transform.position, Vector3.zero);
        var b = renderers[0].bounds;
        foreach (Renderer r in renderers)
        {
            b.Encapsulate(r.bounds);
        }
        return b;
    }
}
