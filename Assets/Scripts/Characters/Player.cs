using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;



public class Player : Character, IShopCustomer, IDamageble, IInmune
{
    public enum characterState
    {
        Alive,
        Die,
        Destroy

    }


    public Animator animator;
    public string name;
    [SerializeField] private int numOfWeapon;
    [SerializeField] public List<ItemSo> inventory;
    public WeaponSO[] weapons = new WeaponSO[5];
    public PlayerDataSO playerData;
    [SerializeField] private int TotalEnemies;
    private bool _isMoving;
    public List<string> Achievements;
    [SerializeField] private bool _canBeDamaged;


    private void Awake()
    {

        SaveInstanceOFPlayerDataBase();
        LoadActualPlayerData();
        LoadFirstWeapon();
        actualHealth = MaxHealth;
        numOfWeapon = 0;
        this.independentMovement = false;
        name = GameManager.PlayerName;
        GameManager.IsPlayerAlive = IsAlive;
        GameManager.PlayerHealth = actualHealth;
        Achievements = GameManager.AchievementsFinnished;
        _canBeDamaged = true;
    }
    void FixedUpdate()
    {
        Move();
    }
    void Update()
    {

        currentPosition = transform.position;
        if (GameManager.GameState == GameStates.Running)
        {
            TotalEnemies = GameManager.TotalEnemies;
            UpdateHealth();
            FollowPointer();
            ChangeWeapon();
            Attack();
            Reload();
            UseItem();
            Die();
        }

    }
    private void UpdateHealth()
    {
        GameManager.PlayerHealth = actualHealth;
    }
    private void FollowPointer()
    {
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.up = mousePos - new Vector2(transform.position.x, transform.position.y);
    }

    public override void Die()
    {
        if (actualHealth <= 0)
        {
            GetComponent<PlayerAudioController>().PlayPlayerDeathAudio();

            GameManager.IsPlayerAlive = false;

        }
    }
    public override void Move()
    {
        if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {

                Vector2 direction = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
                GetComponent<Dash>().DoDash(direction);

            }
            else
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(Input.GetAxis("Horizontal") * speed * Time.deltaTime, Input.GetAxis("Vertical") * speed * Time.deltaTime);
                GetComponent<PlayerAudioController>().PlayPlayerMovementAudio();
            }

            if (!_isMoving)
            {
                _isMoving = true;
                animator.SetBool("IsMoving", _isMoving);

            }

        }
        else
        {
            if (_isMoving)
            {
                _isMoving = false;
                animator.SetBool("IsMoving", _isMoving);
            }
        }
    }

    public override void Attack()
    {

        if (Input.GetMouseButton(0))
        {
            Transform weaponGO = this.gameObject.transform.GetChild(0);
            weaponGO.gameObject.GetComponent<WeaponController>().Shoot();
        }
    }

    public void Reload()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Transform weaponGO = this.gameObject.transform.GetChild(0);
            weaponGO.gameObject.GetComponent<WeaponController>().Reload();
        }
    }
    public void ChangeWeapon()
    {


        if (Input.GetAxis("Mouse ScrollWheel") > 0f || Input.GetKeyDown("c")) // forward
        {

            if (numOfWeapon < 4)
            {

                Transform weaponGO = this.gameObject.transform.GetChild(0);
                weapons[numOfWeapon] = weaponGO.gameObject.GetComponent<WeaponController>().SaveStateOfWeapon(weapons[numOfWeapon]);
                numOfWeapon++;
                weaponGO.gameObject.GetComponent<WeaponController>().LoadWeapon(weapons[numOfWeapon]);

            }
        }

        if (Input.GetAxis("Mouse ScrollWheel") < 0f || Input.GetKeyDown("x")) // backwards
        {
            if (numOfWeapon > 0)
            {

                Transform weaponGO = this.gameObject.transform.GetChild(0);
                weapons[numOfWeapon] = weaponGO.gameObject.GetComponent<WeaponController>().SaveStateOfWeapon(weapons[numOfWeapon]);
                numOfWeapon--;
                weaponGO.gameObject.GetComponent<WeaponController>().LoadWeapon(weapons[numOfWeapon]);


            }

        }
    }
    public void LoadFirstWeapon()
    {
        Transform weaponGO = this.gameObject.transform.GetChild(0);
        weaponGO.gameObject.GetComponent<WeaponController>().LoadWeapon(weapons[0]);
    }
    public void LoadActualPlayerData()
    {

        playerData = Resources.Load<PlayerDataSO>("PlayerDataActual/ActualPlayerDataSO");
        this.name = playerData.Charactername;
        this.IsAlive = playerData.IsAlive;
        this.speed = playerData.speed;
        this.MaxHealth = playerData.Health;
        this.independentMovement = playerData.independentMovement;
        for (int i = 0; i < weapons.Length; i++)
        {
            this.weapons[i] = (WeaponSO)ScriptableObject.CreateInstance(typeof(WeaponSO));
            this.weapons[i].Init(playerData.weapons[i]);
        }

    }
    public void SaveInstanceOFPlayerDataBase()
    {
        if (GameManager.ActualLevel == GameActualLevel.FirstLevel)
        {
            string newAssetPath = "Assets/Resources/PlayerDataActual/ActualPlayerDataSO.asset";
            // AssetDatabase.CreateAsset(playerData, newAssetPath);
            if (AssetDatabase.CopyAsset("Assets/ScriptableObjectes/PlayerData/PlayerDataBase.asset", newAssetPath))
            {
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }

        }
    }


    private void UseItem()
    {
        GameObject ControllerGO = GameObject.Find("Inventary");
        InventaryController controller = ControllerGO.GetComponent<InventaryController>();

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            try
            {
                controller.UseItem(inventory[0]);
                inventory.Remove(inventory[0]);
            }
            catch
            {
                inventory.Remove(inventory[0]);
            }

        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            try
            {
                controller.UseItem(inventory[1]);
                inventory.Remove(inventory[1]);
            }
            catch { }
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            try
            {
                controller.UseItem(inventory[2]);
                inventory.Remove(inventory[2]);
            }
            catch { }
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            try
            {
                controller.UseItem(inventory[3]);
                inventory.Remove(inventory[3]);
            }
            catch { }
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            try
            {
                controller.UseItem(inventory[4]);
                inventory.Remove(inventory[4]);
            }
            catch { }

        }
    }


    public void BoughtItem(ItemSo item)
    {
        if (inventory.Count >= 0 && inventory.Count < 5)
        {

            inventory.Add(item);
        }
        else
        {
            Debug.Log("Don't have space");
        }

    }

    public bool TrySpendGoldAmount(int goldCoins)
    {
        if (GameManager.GoldCoins >= goldCoins)
        {
            if (inventory.Count >= 0 && inventory.Count < 5)
            {
                GameManager.GoldCoins -= goldCoins;
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public void RecieveDamage(int damage)
    {
        if (_canBeDamaged)
        {
            actualHealth -= damage;
        }
    }

    public void BeInmuneForSeconds(int time)
    {
        StartCoroutine(BeInmune(time));
    }
    IEnumerator BeInmune(int time)
    {
        _canBeDamaged = false;
        yield return new WaitForSeconds(time);
        _canBeDamaged = true;

    }
}


