using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Character, IDamageble
{
    public int pointsByDefeating;
    public int coinsByDefeating;
    public AudioClip DyingSound;
    public AudioSource _audioSource;
    public enum EnemyState
    {
        Alive,
        Stunned,
        Die,
        Destroy

    }
    private void Awake()
    {
        GameManager.TotalEnemies++;
        _audioSource = GetComponent<AudioSource>();
    }
    public override void Attack() { }

    public override void Die()
    {
        GameManager.PlayerPoints += pointsByDefeating;
        GameManager.TotalEnemies--;
        GameManager.EnemiesDefeated++;
        GameManager.GoldCoins += coinsByDefeating;
        _audioSource.clip = DyingSound;
        _audioSource.Play();

    }
    public override void Move() { }
    public void RecieveDamage(int damage)
    {
        actualHealth -= damage;
    }
}
