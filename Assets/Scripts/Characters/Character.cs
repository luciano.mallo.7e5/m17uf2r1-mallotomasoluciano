using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character : MonoBehaviour
{
    public int MaxHealth;
    public int actualHealth;
    public int speed;
    public int damage;
    public Vector3 currentPosition;
    public bool independentMovement;
    public bool IsAlive;
    public int experienceForKilling;

    public abstract void Move();
    public abstract void Attack();

    public abstract void Die();


}
