using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goblin : Enemy, IStunnerable
{
    private Transform target;
    private float _impulseForce = 30f;
    public Animator Animator;
    private EnemyState _enemyState;


    void Start()
    {
        _enemyState = EnemyState.Alive;
        this.pointsByDefeating = 50;
        this.coinsByDefeating = 10;
        this.name = "Goblin";
        this.speed = 1;
        this.actualHealth = 40;
        this.currentPosition = transform.position;
        this.damage = 20;
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    void Update()
    {
        switch (_enemyState)
        {
            case EnemyState.Alive:
                FollowPlayer();
                CheckIfStillAlive();
                break;

            case EnemyState.Stunned:
                Debug.Log("Is stunned");
                break;

            case EnemyState.Die:
                _enemyState = EnemyState.Destroy;
                gameObject.GetComponent<Collider2D>().enabled = false;
                gameObject.GetComponent<Rigidbody2D>().isKinematic = false;
                base.Die();
                break;

            case EnemyState.Destroy:
                if (!_audioSource.isPlaying)
                {
                    Destroy(gameObject);
                }
                break;

            default:
                break;
        }

    }

    private void CheckIfStillAlive()
    {
        if (actualHealth <= 0)
        {
            _enemyState = EnemyState.Die;
            Animator.SetBool("IsDead",true);
        }
    }



    public void FollowPlayer()
    {

        transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        Animator.SetFloat("X", -transform.position.x);
        Animator.SetFloat("Y", transform.position.y);
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player" || collision.collider.tag == "Bullet")
        {
            if (collision.collider.tag == "Player")
            {

                GameObject PlayerGO = collision.gameObject;
                PlayerGO.GetComponent<IDamageble>().RecieveDamage(damage);
                Vector2 direction = (Vector2)PlayerGO.transform.position - (Vector2)transform.position;
                PlayerGO.GetComponent<Rigidbody2D>().AddForce(direction * _impulseForce, ForceMode2D.Impulse);
                _enemyState = EnemyState.Die;

            }

        }
    }

    public void Stun(float time)
    {
        StartCoroutine(Stunned(time));
    }
    IEnumerator Stunned(float time)
    {

        _enemyState = EnemyState.Stunned;

        yield return new WaitForSeconds(time);

        _enemyState = EnemyState.Alive;
    }
}
