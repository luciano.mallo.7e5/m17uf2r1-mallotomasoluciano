using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    public Sprite[] DoorSprite;
    private void OnEnable()
    {
        DefeatingAllEnemiesEvent.DefeatAllEnemiesCompleted += ChangeSpriteOfTheDoor;
    }
    private void OnDisable()
    {
        DefeatingAllEnemiesEvent.DefeatAllEnemiesCompleted -= ChangeSpriteOfTheDoor;
    }

    void Start()
    {
        GetComponent<SpriteRenderer>().sprite = DoorSprite[0];
        GetComponent<Collider2D>().isTrigger = false;
    }

    private void ChangeSpriteOfTheDoor()
    {

        GetComponent<SpriteRenderer>().sprite = DoorSprite[1];
        GetComponent<Collider2D>().isTrigger = true;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.tag == "Player")
        {
            GameManager.PlayerPoints += 150;
            GameManager.IsLevelFinished = true;
        }
    }
}
