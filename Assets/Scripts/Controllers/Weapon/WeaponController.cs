using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    private GameObject _bullet;
    [SerializeField] private new string _name;
    private Sprite _weaponSprite;
    [SerializeField] private float _actualAmmo;
    [SerializeField] private float _rowAmmo;
    [SerializeField] private float _totalAmmo;
    [SerializeField] private float _maxAmmo;
    [SerializeField] private float _fireRate;
    [SerializeField] private float _reloadTime;
    [SerializeField] private float _weaponForce;
    [SerializeField] private bool _isReloading;
    [SerializeField] private float _nextTimeToShoot;
    [SerializeField] private int damage;
    [SerializeField] private AudioClip _shootAudio;
    public WeaponSO WeaponSOData;

    private void Update()
    {
        Vector2 mouseWorldSpace = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 direction = mouseWorldSpace - (Vector2)transform.localPosition;
        transform.up = -direction;

    }


    public void Shoot()
    {

        _nextTimeToShoot += Time.deltaTime;

        if (_actualAmmo > 0 && !_isReloading)
        {
            if (Time.time > _nextTimeToShoot)
            {
                _actualAmmo--;
                if (_bullet != null)
                {
                    GameObject bullet = Instantiate(_bullet, transform.position, transform.rotation);
                    bullet.GetComponent<Bullet>().damage = damage;
                    bullet.GetComponent<Bullet>()._force = _weaponForce;
                    GetComponentInParent<PlayerAudioController>().PlayPlayerAttackAudio(_shootAudio);
                  
                }
                _nextTimeToShoot = Time.time + 1 / _fireRate;

            }
        }
        else
        {
            Reload();
        }

    }
    public void Reload()
    {
        if (_totalAmmo > 0)
        {
            StartCoroutine(Reloading());
        }
        else
        {
            return;
        }

    }
    IEnumerator Reloading()
    {
        // Reloading
        _isReloading = true;

        yield return new WaitForSeconds(_reloadTime);
        _isReloading = false;
        if (_actualAmmo == 0)
        {
            _actualAmmo = _rowAmmo;
            _totalAmmo -= _rowAmmo;
        }
        else
        {
            _totalAmmo -= _rowAmmo - _actualAmmo;
            _actualAmmo += _rowAmmo - _actualAmmo;
        }

    }
    public void LoadWeapon(WeaponSO NewWeaponSOData)
    {

        this._bullet = NewWeaponSOData.Bullet;
        this._name = NewWeaponSOData.Name;
        this._actualAmmo = NewWeaponSOData.ActualAmmo;
        this._rowAmmo = NewWeaponSOData.ActualAmmo;
        this._totalAmmo = NewWeaponSOData.TotalAmmo;
        this._maxAmmo = NewWeaponSOData.MaxAmmo;
        this._fireRate = NewWeaponSOData.FireRate;
        this._reloadTime = NewWeaponSOData.ReloadTime;
        this._isReloading = NewWeaponSOData.IsReloading;
        this._weaponForce = NewWeaponSOData.WeaponForce;
        this._nextTimeToShoot = NewWeaponSOData.NextTimeToShoot;
        this.damage = NewWeaponSOData.Damage;
        this._shootAudio = NewWeaponSOData.WeaponAudio;
        LoadWeaponSprite(NewWeaponSOData.ItemSprite);
    }

    public WeaponSO SaveStateOfWeapon(WeaponSO LastWeaponSOData)
    {
        // LastWeaponSOData.Bullet = this._bullet;
        // LastWeaponSOData.Name = this._name;
        LastWeaponSOData.ActualAmmo = this._actualAmmo;
        LastWeaponSOData.ActualAmmo = this._rowAmmo;
        LastWeaponSOData.TotalAmmo = this._totalAmmo;
        LastWeaponSOData.MaxAmmo = this._maxAmmo;
        LastWeaponSOData.FireRate = this._fireRate;
        LastWeaponSOData.ReloadTime = this._reloadTime;
        // LastWeaponSOData.IsReloading = this._isReloading;
        // LastWeaponSOData.WeaponForce = this._weaponForce;
        // LastWeaponSOData.Damage = this.damage;

        return LastWeaponSOData;
    }
    private void LoadWeaponSprite(Sprite weaponSprite)
    {
        if (weaponSprite != null) GameObject.Find("WeaponSprite").GetComponent<SpriteRenderer>().sprite = weaponSprite;

    }

    public void AddAmmo(int nunOfAmmo)
    {
        if (_totalAmmo < _maxAmmo)
        {
            if (_totalAmmo + nunOfAmmo > _maxAmmo)
            {
                _totalAmmo = _maxAmmo;
            }
            else
            {
                _totalAmmo += nunOfAmmo;
            }
        }


    }

    public string GetWeaponName() => _name;
}
