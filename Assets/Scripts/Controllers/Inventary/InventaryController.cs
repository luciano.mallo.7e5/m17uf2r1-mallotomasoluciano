using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventaryController : MonoBehaviour
{
    // public ItemSo item;
    private GameObject _player;
    private void Awake()
    {
        _player = GameObject.FindGameObjectWithTag("Player");
    }

    public void UseItem(ItemSo item)
    {
        switch (item.Name)
        {
            case "HealthPotion":
                AddHp(20);
                break;
            case "InmunePotion":
                MakePlayerInmune(5);
                break;
            default:
                Debug.Log("No item to use");
                break;
        }
        item.ItemSound.Play();
    }
    private void AddHp(int hp)
    {

        if (GameManager.PlayerHealth < _player.GetComponent<Character>().MaxHealth)
        {

            if (GameManager.PlayerHealth + hp > _player.GetComponent<Character>().MaxHealth)
            {

                _player.GetComponent<Character>().actualHealth = _player.GetComponent<Character>().MaxHealth;
            }
            else
            {

                _player.GetComponent<Character>().actualHealth += hp;

            }
        }
    }
    private void MakePlayerInmune(int time) 
    {
        GameObject.FindGameObjectWithTag("Player").GetComponent<IInmune>().BeInmuneForSeconds(time);
    }
}
