using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class PlayAgain : MonoBehaviour
{
    [SerializeField] Button _playAgainButton;
    private void Awake()
    {
        _playAgainButton = GameObject.Find("PlayAgainButton").GetComponent<Button>();
        _playAgainButton.onClick.AddListener(LoadStartScene);
    }
    private void LoadStartScene() 
    {
        SceneManager.LoadScene("StartScene");
    }
}
