using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenesController : MonoBehaviour
{

    void Update()
    {

        if (GameManager.IsLevelFinished && GameManager.IsPlayerAlive)
        {
            switch (GameManager.ActualLevel)
            {
                case GameActualLevel.StartMenu:
                    GameManager.LevelsDone = 1;
                    GameManager.ActualLevel = GameActualLevel.FirstLevel;
                    GameManager.IsLevelFinished = false;
                    SceneManager.LoadScene("LevelAutoCreated");
                    break;
                case GameActualLevel.FirstLevel:
                    GameManager.LevelsDone++;
                    GameManager.ActualLevel = GameActualLevel.SecondLevel;
                    GameManager.IsLevelFinished = false;
                    SceneManager.LoadScene("LevelAutoCreated");
                    break;

                case GameActualLevel.SecondLevel:
                    GameManager.LevelsDone++;
                    GameManager.ActualLevel = GameActualLevel.ThirdLevel;
                    GameManager.IsLevelFinished = false;
                    SceneManager.LoadScene("LevelAutoCreated");
                    break;

                case GameActualLevel.ThirdLevel:
                    GameManager.ActualLevel = GameActualLevel.Finish;
                    SceneManager.LoadScene("FinishScene");
                    break;
            }

        }
        else if (!GameManager.IsPlayerAlive)
        {
            SceneManager.LoadScene("FinishScene");
        }

    }
}
