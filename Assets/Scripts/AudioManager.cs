using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum MusicByLevel 
{
    StartMenu,
    OnGameMusic
}
public class AudioManager : MonoBehaviour
{
    public static AudioManager _audioManager;
    public AudioMixer _masterMixer;
    [SerializeField] private Slider Slider;
    [SerializeField] private AudioClip[] _onGameMusicPlayList;
    [SerializeField] private AudioClip[] _startMenuPlayList;
    [SerializeField] private SoundsSO[] _musicPlayListSO;
    private MusicByLevel _musicByLevel;

    private AudioSource _audioSource;

    private void Awake()
    {
        if (_audioManager != null && _audioManager != this)
            Destroy(this.gameObject);
        _audioManager = this;

        _audioSource = GetComponent<AudioSource>();
        _startMenuPlayList = _musicPlayListSO[0].audioClips;
        _onGameMusicPlayList = _musicPlayListSO[1].audioClips;
        _musicByLevel = MusicByLevel.StartMenu;
       
        DontDestroyOnLoad(this);
    }
    private void Update()
    {
        PlayeBackgroundMusic();
    }


    private AudioClip GetRandomClip(AudioClip[] audioClips)
    {
        return audioClips[Random.Range(0, audioClips.Length)];
    }

    public void SetMasterVolume(Slider volume)
    {
        _masterMixer.SetFloat("Master", volume.value);
    }

    public void SetMusicVolume(Slider volume)
    {
        _masterMixer.SetFloat("Music", volume.value);
    }

    public void SetFXVolume(Slider volume)
    {
        _masterMixer.SetFloat("FX", volume.value);
    }




    private void PlayeBackgroundMusic()
    {
        switch (GameManager.ActualLevel)
        {
            case GameActualLevel.StartMenu:
                if (!_audioSource.isPlaying || _musicByLevel != MusicByLevel.StartMenu)
                {
                    _musicByLevel = MusicByLevel.StartMenu;
                    _audioSource.clip = GetRandomClip(_startMenuPlayList); ;
                    _audioSource.Play();
                }
                break;

            case GameActualLevel.FirstLevel:
            case GameActualLevel.SecondLevel:
            case GameActualLevel.ThirdLevel:
                if (!_audioSource.isPlaying || _musicByLevel != MusicByLevel.OnGameMusic)
                {
                    _musicByLevel = MusicByLevel.OnGameMusic;
                    _audioSource.clip = GetRandomClip(_onGameMusicPlayList);
                    _audioSource.Play();
                }
                break;
            case GameActualLevel.Finish:
                if (!_audioSource.isPlaying || _musicByLevel != MusicByLevel.StartMenu)
                {
                    _musicByLevel = MusicByLevel.StartMenu;
                    _audioSource.clip = GetRandomClip(_startMenuPlayList);
                    _audioSource.Play(); 
                }
                
                break;
            default:
                _musicByLevel = MusicByLevel.StartMenu;
                _audioSource.clip = GetRandomClip(_startMenuPlayList); ;
                _audioSource.Play();
                break;
        }

    }
}


