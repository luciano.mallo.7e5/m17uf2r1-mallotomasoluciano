using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievementsManager : MonoBehaviour
{
    public delegate void AchievementDone();
    public static event AchievementDone AchievementCompleted;
    [SerializeField] private List<string> Achievements = new List<string>();
    // public List<AchievementsSO> AchievementsSO = new List<AchievementsSO>(); to be implemented

    private void Awake()
    {
        Achievements.Add("DefeatFiveEnemies");
        Achievements.Add("CollectHundredGoldCoin");
    }
    private void Update()
    {
        if (GameManager.EnemiesDefeated >=5  && Achievements.Contains("DefeatFiveEnemies"))
        {
            Achievements.Remove("DefeatFiveEnemies");
            GameManager.AddAnAchievementsFinnished("DefeatFiveEnemies");
            AchievementCompleted();
        }
        if (GameManager.GoldCoins > 100 && Achievements.Contains("CollectHundredGoldCoin"))
        {
            Achievements.Remove("CollectHundredGoldCoin");
            GameManager.AddAnAchievementsFinnished("CollectHundredGoldCoin");
            AchievementCompleted();
        }
    }
}
