using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefeatingAllEnemiesEvent : MonoBehaviour
{
    public delegate void DefeatAllEnemies();
    public static event DefeatAllEnemies DefeatAllEnemiesCompleted;

    private void Update()
    {
        if (GameManager.TotalEnemies == 0)
        {
            DefeatAllEnemiesCompleted();
        }

    }
}
