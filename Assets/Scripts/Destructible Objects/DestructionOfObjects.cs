using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructionOfObjects : MonoBehaviour
{
    

    private void OnCollisionEnter2D(Collision2D collision)
    {

        
        if (collision.collider.tag == "Bullet")
        {
            GetComponent<LootBag>().InstaiateLoot(transform.position);
            Destroy(gameObject);
        }

    }

}
