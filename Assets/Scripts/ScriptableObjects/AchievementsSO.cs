using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AchievementsSO", menuName = "ScriptableObjects/AchievementsSO", order = 1)]
public class AchievementsSO : ScriptableObject
{
    public string AchievementName;
    public Sprite AchievementSprite;
}
