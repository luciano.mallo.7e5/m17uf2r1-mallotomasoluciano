using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PotionSO", menuName = "ScriptableObjects/Items/PotionSO", order = 1)]
public class PotionSO : ItemSoNone
{
    
    public string Type;
    public int EffectDuration;
    public string Description;

}
