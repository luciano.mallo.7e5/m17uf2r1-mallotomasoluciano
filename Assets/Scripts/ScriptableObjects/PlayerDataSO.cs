using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "PlayerData", menuName = "ScriptableObjects/PlayerData", order = 1)]
public class PlayerDataSO : ScriptableObject
{
    public string Charactername;
    public int Health;
    public int speed;
    public bool independentMovement;
    public bool IsAlive;
    public Animator animator;
    public ItemSo[] inventory;
    public WeaponSO[] weapons;

    public void LoadData(PlayerDataSO playerData) 
    {

        this.Charactername = playerData.Charactername;
        this.Health= playerData.Health;
        this.speed= playerData.speed;
        this.independentMovement= playerData.independentMovement;
        this.IsAlive= playerData.IsAlive;
        this.animator=playerData.animator;
        this.inventory=playerData.inventory;
        this.weapons=playerData.weapons;
}

}
