using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Inventory", menuName = "ScriptableObjects/Item", order = 1)]
public class ItemSoNone : ScriptableObject
{
    public string Name;
    public Sprite ItemSprite;
    public int Price;


}
