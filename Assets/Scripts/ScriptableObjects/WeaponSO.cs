using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Weapon", menuName = "ScriptableObjects/Weapon", order = 1)]
public class WeaponSO : ItemSoNone
{
   
    public GameObject Bullet;
    public float ActualAmmo;
    public float RowAmmo;
    public float TotalAmmo;
    public float MaxAmmo;
    public float FireRate;
    public float ReloadTime;
    public bool IsReloading;
    public float NextTimeToShoot;
    public int Damage;
    public float WeaponForce;
    public AudioClip WeaponAudio;


    public void Init(WeaponSO weaponSOToLoad)
    {
        this.Name = weaponSOToLoad.Name;
        this.ItemSprite = weaponSOToLoad.ItemSprite;
        this.Bullet = weaponSOToLoad.Bullet;
        this.ActualAmmo = weaponSOToLoad.ActualAmmo;
        this.RowAmmo = weaponSOToLoad.RowAmmo;
        this.TotalAmmo = weaponSOToLoad.TotalAmmo;
        this.MaxAmmo = weaponSOToLoad.MaxAmmo;
        this.FireRate = weaponSOToLoad.FireRate;
        this.ReloadTime = weaponSOToLoad.ReloadTime;
        this.IsReloading = weaponSOToLoad.IsReloading;
        this.Damage = weaponSOToLoad.Damage;
        this.WeaponForce = weaponSOToLoad.WeaponForce;
        this.WeaponAudio = weaponSOToLoad.WeaponAudio;
    }

}
