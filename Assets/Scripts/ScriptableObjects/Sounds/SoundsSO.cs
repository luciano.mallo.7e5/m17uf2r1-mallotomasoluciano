using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SoundsSO", menuName = "ScriptableObjects/SoundsSO", order = 1)]
public class SoundsSO : ScriptableObject
{
    public AudioClip[] audioClips;
}
