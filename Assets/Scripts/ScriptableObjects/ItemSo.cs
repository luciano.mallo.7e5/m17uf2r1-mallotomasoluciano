using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "ItemSo", menuName = "ScriptableObjects/Item", order = 1)]
public class ItemSo : ScriptableObject
{
    public string Name;
    public Sprite ItemSprite;
    public int Price;
    public int dropChance;
    public AudioSource ItemSound;

    public ItemSo(string lootName, int dropChance, Sprite itemSprite, int price, AudioSource itemSound)
    {
        this.Name = lootName;
        this.dropChance = dropChance;
        this.ItemSprite = itemSprite;
        this.Price = price;
        this.ItemSound = itemSound;
    }

    public ItemSo Init(ItemSo itemSo)
    {
        this.Name = itemSo.Name;
        this.dropChance = itemSo.dropChance;
        this.ItemSprite = itemSo.ItemSprite;
        this.Price = itemSo.Price;
        this.ItemSound = itemSo.ItemSound;
        return this;
    }
}