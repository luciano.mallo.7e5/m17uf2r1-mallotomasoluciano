using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AutoCreateLevel : MonoBehaviour
{
    public static AutoCreateLevel AutoCreateLevelSingleton;
    public int x, y;
    private int[,] gameMatrix;
    private int[,] matrixBlocks;
    public GameObject RoomToInstantiate;
    public GameObject FinnishRoomToInstantiate;
    [HideInInspector] public int finnishPoint;
    [HideInInspector] public int startPoint;


    private void Awake()
    {
        if (AutoCreateLevelSingleton == null)
        {
            AutoCreateLevelSingleton = this;
        }
        else
        {
            DestroyImmediate(gameObject);
        }

       

        CreateWorld();


    }

    void CreateWorld()
    { 
        switch (GameManager.ActualLevel)
        {
            case GameActualLevel.FirstLevel:
                x = 10;
                y = 5;
                break;

            case GameActualLevel.SecondLevel:
                x = 15;
                y = 10;
                break;

            case GameActualLevel.ThirdLevel:
                x = 20;
                y = 15;
                break;
        }
        gameMatrix = new int[x, y];
        matrixBlocks = new int[x, y];
        MakeRoute();
        DeterminateBlocks();
        CreateBlocks();
    }
    private void OnDrawGizmos()
    {
        if (gameMatrix == null)
        {
            return;
        }
        for (int i = 0; i < x; i++)
        {
            for (int j = 0; j < y; j++)
            {
                switch (gameMatrix[i, j])
                {
                    case 0:
                        Gizmos.color = Color.black;
                        break;
                    case 1:
                        Gizmos.color = Color.green;
                        break;
                    case 2:
                        Gizmos.color = Color.red;
                        break;
                    case 3:
                        Gizmos.color = Color.yellow;
                        break;
                    default:
                        break;

                }


                // Gizmos.DrawCube(new Vector3(i * 10, j * 10, 0), Vector3.one * 10);
            }
        }
    }
    void MakeRoute()
    {
        startPoint = Random.Range(0, x);
        finnishPoint = Random.Range(0, x);


        int[] tracer = { startPoint, y - 1 };
        while (!(tracer[0] == finnishPoint && tracer[1] == 0))
        {
            int movement;
            bool _repeat = false;
            do
            {
                _repeat = false;

                movement = Random.Range(1, 4);
                switch (movement)
                {
                    case 1:
                        tracer[0]++;
                        if (tracer[0] >= x)
                        {
                            tracer[0]--;
                            _repeat = true;
                        }
                        break;
                    case 2:
                        tracer[1]--;
                        if (tracer[1] < 0)
                        {
                            tracer[1]++;
                            _repeat = true;
                        }
                        break;
                    case 3:
                        tracer[0]--;
                        if (tracer[0] < 0)
                        {
                            tracer[0]++;
                            _repeat = true;
                        }
                        break;
                    default:
                        break;
                }
            } while (_repeat);

            gameMatrix[tracer[0], tracer[1]] = 3;
        }
        gameMatrix[startPoint, y - 1] = 1;
        gameMatrix[finnishPoint, 0] = 2;


        GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>().position = new Vector3(startPoint * 10, (y - 1) * 10, 0);
        GameObject.FindGameObjectWithTag("Boss").GetComponent<Transform>().position = new Vector3(finnishPoint * 10, 0, 0);

    }

    void DeterminateBlocks()
    {

        for (int i = 0; i < x; i++)
        {
            for (int j = 0; j < y; j++)
            {
                int result = 0;
                result += 1 * CheckIfThereIsABlock(i, j + 1);
                result += 2 * CheckIfThereIsABlock(i + 1, j);
                result += 4 * CheckIfThereIsABlock(i, j - 1);
                result += 8 * CheckIfThereIsABlock(i - 1, j);
                matrixBlocks[i, j] = result;
            }
        }

    }

    private void CreateBlocks()
    {
        for (int i = 0; i < x; i++)
        {
            for (int j = 0; j < y; j++)
            {
                if (gameMatrix[i, j] != 0)
                {
                    GameObject bl;



                    if (gameMatrix[startPoint, y - 1] == gameMatrix[i, j])
                    {
                        bl = Instantiate(RoomToInstantiate, new Vector3(i * 10, j * 10, 0), Quaternion.identity) as GameObject;
                        bl.name = "StartRoom";
                        bl.tag = "StartRoom";
                    }
                    else if(gameMatrix[finnishPoint, 0] == gameMatrix[i, j])
                    {
                        bl = Instantiate(FinnishRoomToInstantiate, new Vector3(i * 10, j * 10, 0), Quaternion.identity) as GameObject;
                        bl.name = "BossRoom";
                        bl.tag = "BossRoom";
                       
                    }
                    
                    else
                    {
                        bl = Instantiate(RoomToInstantiate, new Vector3(i * 10, j * 10, 0), Quaternion.identity) as GameObject;
                        bl.name = "Room";
                    }

                    bl.GetComponent<Blocks>().Initializer(matrixBlocks[i, j]);
                    bl.transform.SetParent(this.transform);
                    if (bl != null)
                    {
                        bl.GetComponent<PutDestructibleObjects>().PutObjects(bl);
                    }
                }

            }
        }
    }

    private int CheckIfThereIsABlock(int _x, int _y)
    {
        if (_x < 0 || _y < 0 || _x >= x || _y >= y)
        {

            return 0;
        }
        if (gameMatrix[_x, _y] == 0)
        {
            return 0;
        }
        return 1;
    }
}
