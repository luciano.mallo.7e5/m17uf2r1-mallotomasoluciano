using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blocks : MonoBehaviour
{

    private int _doorToActivateOrDesactivate;
    public GameObject[] Doors;

    private void Start()
    {

    }
    public void Initializer(int positionOfDoor)
    {
        _doorToActivateOrDesactivate = positionOfDoor;
        Doors[0].SetActive((_doorToActivateOrDesactivate % 2) != 1);
        Doors[1].SetActive((Mathf.FloorToInt(_doorToActivateOrDesactivate / 2) % 2) != 1);
        Doors[2].SetActive((Mathf.FloorToInt(_doorToActivateOrDesactivate / 4) % 2) != 1);
        Doors[3].SetActive((Mathf.FloorToInt(_doorToActivateOrDesactivate / 8) % 2) != 1);
    }
}
