using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PutDestructibleObjects : MonoBehaviour
{
    public GameObject[] DestructibleObjectsToPut;
    private int probability;
    private GameActualLevel actualLevel;
    private int _probabilityByLevel;

    public void PutObjects(GameObject GM)
    {
        
        CheckLevel();
        probability = Random.Range(0,101);
        if (probability > _probabilityByLevel) 
        { 
                Instantiate(DestructibleObjectsToPut[0], WhereToPut(GetMaxBounds(GM)), Quaternion.identity);
        }
    }

    private Bounds GetMaxBounds(GameObject g)
    {
        var renderers = g.GetComponentsInChildren<Renderer>();
        if (renderers.Length == 0) return new Bounds(g.transform.position, Vector3.zero);
        var b = renderers[0].bounds;
        foreach (Renderer r in renderers)
        {
            b.Encapsulate(r.bounds);
        }
        return b;
    }

    private Vector3 WhereToPut(Bounds gm)
    {
        var bounds = gm;
        var x = bounds.center.x + Random.Range(-bounds.extents.x / 2, bounds.extents.x / 2);
        var y = bounds.center.y + Random.Range(-bounds.extents.y / 2, bounds.extents.y / 2);
        return new Vector3(x, y, 0);
    }
    private void CheckLevel() {
        actualLevel = GameManager.ActualLevel;
        switch (actualLevel)
        {
            case GameActualLevel.FirstLevel:
                _probabilityByLevel = 65;
                break;
            case GameActualLevel.SecondLevel:
                _probabilityByLevel = 50;
                break;
            case GameActualLevel.ThirdLevel:
                _probabilityByLevel = 25;
                break;
        }
    }
}
