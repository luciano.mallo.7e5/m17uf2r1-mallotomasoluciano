using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Shop : MonoBehaviour
{
    private Transform container;
    private Transform shopItemTemplate;
    public List<ItemSo> SellingItems = new List<ItemSo>();
    private IShopCustomer shopCustomer;
    private bool _noEnoughCoind;

    private void Awake()
    {
        container = transform.Find("Container");
        shopItemTemplate = container.Find("ShopItemTemplate");
        shopItemTemplate.gameObject.SetActive(false);

    }
    private void Start()
    {
        for (int i = 0; i < SellingItems.Count; i++)
        {
            CreateItemButton(SellingItems[i], i);
        }
    }
    private void CreateItemButton(ItemSo item, int positionIndex)
    {


        Transform shopItemTransform = Instantiate(shopItemTemplate, container);
        shopItemTransform.gameObject.SetActive(true);
        RectTransform shopItemRectTransform = shopItemTransform.GetComponent<RectTransform>();
        float shopItemHeight = 35f;
        shopItemRectTransform.anchoredPosition = new Vector2(0, -shopItemHeight * positionIndex);

        shopItemTransform.Find("NameText").GetComponent<Text>().text = item.Name;
        shopItemTransform.Find("CostText").GetComponent<Text>().text = item.Price.ToString();
        shopItemTransform.Find("ItemImage").GetComponent<Image>().sprite = item.ItemSprite;
        shopItemTransform.GetComponent<Button>().onClick.AddListener(() =>
        {
            TryBuyItem(item);
            if (_noEnoughCoind)
            {
                shopItemTransform.Find("CostText").GetComponent<Text>().color = Color.red;

            }
            else
            {
                shopItemTransform.Find("CostText").GetComponent<Text>().color = Color.black;
            }
        });
    }
    private void TryBuyItem(ItemSo item)
    {
        if (shopCustomer.TrySpendGoldAmount(item.Price))
        {

            shopCustomer.BoughtItem(item);
            _noEnoughCoind = false;
        }
        else
        {
            _noEnoughCoind = true;
        }

    }


    public void Show(IShopCustomer shopCustomer)
    {

        this.shopCustomer = shopCustomer;
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }


}
