# M17 - UF2 - R2 - Mallo Tomaso Luciano Ezequiel

## Lore

The history is about a warrior that enters on a dungeon, trying to beat the mages at the end of the three dungeon. There are different types of enemies, the goblins are invoked by a mage from inside, they are always coming in bunches, all always follow the player, don't keep them to catch you or they will explode. Otherwise, there is a turret that's it's also an enemy, it follows with its gun the warrior trying to beat down him. When the player kills all the enemies the gate of the dungeon will be open and the player can go forward to the next dungeon.

## Instructions
- **Moving**

The player can be move up, down, right and left with the arrows on the keyboard or with keys [ASDW]. With the mouse pointer can point at the enemy, and then pressing the left click can shoot.

- **Shooting and aiming**

With the mouse scroll and the key [X], [C] the weapon select can be change by other if the player got another one.

- **Dash**

When pressing the space bar key the player can do a dash toward the direction to where it's going, while is dashing the player cannot get damage.

- **Shop and Achievements**

During the game the player can press the key [U] to open the shop and the key [I] to open the achievements(if there aren't done, will be shown with a black shadow).

- **How to pause the game?**

If the player press the [Esc] button of the keyboard the settings panel to manage the levels of the fx and music. 

- **How to Play the game?**

If the player is new can press the help button and the settings panel to see the controls.

## Destructible Objects

There is objects than can be destroy by hitting them, when destroying one of them there a probably that drop an item, or just nothing (Good Luck).

## To finish the game

There must beat all the enemies on the screen and cross the gate at the last room.

## Enemies

- Goblins
  - Behavior: It follows the player till it's beat or explode by touching the player.
    - HP: 40
    - Points by beaten: 50
    - Damage: 20
  
- Turret
  - Behavior: When the player enters on the range of the turret it will shoot bullets till the player beat it, the player die or get out of it's range.
    - HP: 200
    - Points by beaten: 100
    - Damage: 10
- Mage
  - Behavior: When the player enters on the range of the mage it will shoot fireballs and teleporting till the player beat it, the player die.
    - HP: 80
    - Points by beaten: 100
    - Damage: 20

## Weapons

- **Name**: Gun
  - RowAmmo: 6
  - MaxAmmo: 30
  - FireRate: 3
  - ReloadTime: 3
  - Damage: 10
  - WeaponForce: 5

- **Name**: Shootgun
  - RowAmmo: 4
  - MaxAmmo: 24
  - FireRate: 4
  - ReloadTime: 4
  - Damage: 20
  - WeaponForce: 10

- **Name**: Baloon
  - this weapon has the power to stun enemies when the baloon touch an enemy.
    - RowAmmo: 3
    - MaxAmmo: 10
    - FireRate: 1
    - ReloadTime: 2
    - Damage: 5
    - WeaponForce: 30

## Items

- **Health Potion**
  - Behavior: When collecting it restore a num of hp of the player. If the hp of the player don't has effect.

- **Inmune Potion**
  - Behavior: They can be get it in the shop. When the player drink it will be inmune for 3 seconds.

- **Gold Coin**
  - Behavior: Add a num point to the counter of the player.

- **Weapon**
  - Behavior: Add the weapon drop to the player inventory, or if already have it add ammo to that weapon.

---

## To Play

It's need to load the *StartScene* escene.
Then the player has to put his/her name and press the *Give Me Rogue Like* button.
When the game starts a numbers of room are generated and the enemies and loot box are drop randomly (So each game will be different). When the game is finished the player can hit the play again button to start a new try.

When the game starts The Player Data it's copy from a base scriptable Object on the scriptable objects folder and saved on the resources folder. Then it's the actual player data loads all that it's need for this level from the copy scriptable Object. So it's something goes wrong always can be load it again from the base, and for the next level data from this one will be saved on the copy to load later.

---
