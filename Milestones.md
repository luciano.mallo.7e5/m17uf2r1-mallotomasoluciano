# M17UF2R2 Gameplay, escalatge i botiga

---

En aquesta entrega acabarem de donar forma el joc. Afegint pantalles, creant un sistema de nivells de tipus rogue like, realitzarem el sistema de compra dele ítems i crearem el sistema d'assoliments i recompenses (achievements).
Recordeu que la següent llista de punts a crear no té definit fins al detall el seu comportament. D'aquesta forma podeu donar el toc més personal al vostre joc. Si hi ha alguna mecànica o punt que voleu canviar per fer-lo radicalment diferent primer heu d'explicar-m'ho per donar-vos el ok.

---

## Llistat de punts a desenvolupar

### Requisits generals

- Utilització d’events per algunes funcionalitats.
- Ús de Scriptable Objects.
- Utilització de Corrutines (IEnumerators) i yields.
  
#### R2.1 Power Ups i realització d'una jerarquia de items

- Obligatori l'ús de Scriptable Objects. (6p)
- Tots els ítems del Player han d'estar dins d'aquesta jerarquia de classes (armes, vida extra, bales...). (6p)
- S'adjunta diagrama de classes de referència. *

#### R2.2 Botiga on comprar items i elements

- Mínim tipus d'elements: armes i power-ups.(5p).
- Tot element de la botiga es pot comprar amb moneda aconseguida en partides anteriors i ha d'aplicar-se a la següent partida. (6p).
- ***(extra)*** Modificacions estètiques. (1p)

- S'adjunta un layout i un diagrama de flux de referència.

#### R2.3 Sistema de persistència de dades

  Persistència entre sessions: guardat en disc.

- Guardat de
  - Últimes puntuacions (el que es mostra a la UI GameOver). (4p)
  - Millor puntuació. (4p)
  - ***(extra)*** Inventaris adquirits. (2p)
  - ***(extra)*** Assoliments. (2p)
  - ***(extra)*** Altres variables que el vostre joc necessiti: sales superades, temps jugat… (1p)

#### R2.4 Afegits al player

- Atac a distància
  - Arma aturdidora: no elimina l’enemic però el per a durant un temps T. (5p)
  - ***(extra)*** llançaflames (versió pacífica manguera):
  - Atac en àrea cònica: per exemple polygon collider 2D (2p)
  - Es pot utilitzar per la part gràfica de la flama tant Sprites com Partícules. (2p)
  - ***(extra)*** L'enemic no mor en el moment de tocar les flames. Ha de rebre una quantitat d'impacte de flames per morir. (2p)
  - ***(extra)*** Bomba de mà o granada de mà. Objecte llençat que, passat un temps T, explota afectant tant a jugador com enemics. Aplicació de força repulsiva. (2p)
- ***(extra)*** Atac a melee:
  - Basant-se en el sistema de melee de l'entrega anterior crear noves armes. (1p)
  - Mínim 1 arma nova. Diferenciar el comportament entre les diferents armes (pot ser res més de variables). (1p)

#### R2.5 Música i so

- Afegir efectes de so: (4p)
- Mort del jugador
- Mort dels enemics (pot ser el mateix so)
- Música de fons:
- Afegir una llista de música de fons. (4p)
- Canvi de música segons el nivell o el moment del joc. (6p)
- ***(extra)*** música d’estat urgència (quan el jugador té poca vida), pot ser acompanyat amb un efecte de parpelleig de la pantalla. (2p)
- Utilitzar l'objecte audiomixer. (4p)

#### R2.6 Progressió del joc: Sistema procedural Rogue Like

- Creació de mínim 3 sales diferents (No es tindrà en compte el level design, sinò la funcionalitat). (6p)
- Generació del següent nivell aleatori, tant en posició com en tipus de sala. (6p)
- Canvi de comportament dels spawners: canviar el lloc (o anul·lar/activar), tipus d'enemic o freqüència d'onada (mínim 1 variació). (6p).
- Extra de puntuació per sala acabada. (4p).
- Utilització d’events per funcionalitats per l'obertura de portes per passar al següent nivell (relacionar claus/nombre d’enemics morts/temps amb l’obertura de la porta). (6p).
- No es pot passar a la següent sala fins que no s’han eliminat tots els enemics del nivell.

#### R2.7 UI

- Pantalla d'inici de joc: (6p)
  - Mostra millor puntuació fins al moment(persistència de dades).
  - Mostra dades de la última partida jugada(persistència de dades).
  - Botons de Jugar i de configuració
- Pantalla de Configuració: (6p)
  - Nivells de so i música
  - Emmudir efectes de so o la música
- Pantalla de GameOver: (6p)
  - Mostra puntuació final
  - Mostra millor puntuació:
  - En cas que sigui la partida amb millor puntuació s'ha de mostrar d'alguna forma (efectes, cartell, etc).
  - Mostra número d'enemics morts.
  - Mostra número de sales superades.

#### R2.8 ***(extra)*** Sistema d'assoliments: (POC PRIORITARI)

- Mínim 4 assoliments activats de formes diferents: destruir XX enemics, Assoliment de combos... (2p).
- Events en partida que activen els assoliments. (2p).
- Sistema d'avís dels assoliments en partida. (2p).
- Registre amb Scriptable Objects. (2p).

#### R2.9 ***(extra)*** Millores de gameplay o tasques de l'anterior entrega

- Podeu fer millores de tot tipus enfront dels punts demanats al repte: Rogue Like Prototype I.(3p).
- Escriure en el *Readme* un llistat d'allò que heu millorat d'una entrega una altra... (1p)

- Es poden fer servir recursos de tercers sempre i quan es respecti els seus drets d’ús. També es poden fer servir recursos propis.

---

*Nota:* en aquesta carpeta teniu una llibreria d’imatges d’elements de UI’s adquirits per l’Institut que podeu utilitzar lliurement. Són fitxer tipus zip on els elements han sigut reunits per estils.

- Repositori de UI’s.
- Full resum de les UI’s.
Tots aquests elements seran utilitzats més endavant en un joc més gran. Per tant dissenyeu la seva arquitectura i codi pensant en escalabilitat i level design. En aquest punt feu ús correcte de l'accessibilitat a les variables.

---

#### Format d’entrega

---

- Entrega en repositori GIT en el núvol
- Nom del projecte: M17UF2R1-CognomsNom
- Documentació en el Readme del projecte. Instrccions i millores dels jocs.
- Diagrames de classes del vostre projecte.
- Commit a la branca master a la plataforma GITLAB amb nom: RogueLike2-Entrega

---
